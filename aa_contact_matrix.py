import numpy as np
import json
import os

np.set_printoptions(threshold=np.inf)

def GetDataFromJSONFile(PDB_id):
    """gets data from a JSON file with a filename containing PDB_id
    PDB_id - an id of a structure which we have some analytic data for"""
    filename = "{}_neighbours.json".format(PDB_id)
    path = os.path.join(os.getcwd(), filename)
    if os.path.exists(path):
        with open(path, 'r') as f:
            data = json.load(f)
        return data    

def CreateZeroMatrix(data, chain):
    """creates a matrix of zeros with the same number of rows and columns
    as the number of residues in the given chain of a PDB structure
    data - a dictionary containing PDB structure data
    chain - a specific chain in a PDB structure"""
    size = data["chain"][chain]["res_num"]
    zero_matrix = np.zeros((size, size), dtype = int)
    return zero_matrix

def CompleteWithContacts(data, zero_matrix):
    """completes the given zero matrix with contacts between aminoacids
    data - a list containing data about aminoacids neighbours for a specific chain
    zero_matrix - a matrix of zeros with a size of a number of aminoacids"""
    
    contact_matrix = np.copy(zero_matrix)
    for aa in data:
        aa_num = int(aa[0])
        for neighbour in aa[1]['neighbours']:
            neigh_num = int(neighbour)
            if aa_num-1 == neigh_num or aa_num+1 == neigh_num: # pomija sasiadow sekwencyjnych
                pass
            else:
                contact_matrix[aa_num-1][neigh_num-1] = +1
                contact_matrix[neigh_num-1][aa_num-1] = +1 # indeksy w macierzy zaczynaja sie od 0
    return contact_matrix

def TransformIntoVector(contact_matrix):
    """transforms the given contact matrix into 1-D vector
    contact_matrix - a matrix where 0 means no contact between aminoacids
                     and 1 means there is a contact between them"""
    vector = contact_matrix.flatten()
    vector = str(vector).strip('[]')
    return vector

def AppendToFASTAFile(PDB_id, chain, vector):
    """appends vector containing data about contacts between aminoacids
    in the given chain of the given PDB structure
    PDB_id - an id of a structure which we have some analytic data for
    chain - a specific chain in a PDB structure
    vector - 1-D array containing 0s & 1s (1 - contact, 0 - no contact)"""
    
    with open('protein_structures.fasta', 'a') as f:
        f.write('>{}_{}\n'.format(PDB_id, chain))
        f.write(vector + '\n')
    
def AminoAcidsContacts(PDB_id):
    """makes FASTA file containing data about contacts between aminoacids
    obtained from JSON file with a PDB id in a filename
    PDB_id - a part of a filename for a JSON file which we want to obtain data from"""
    data = GetDataFromJSONFile(PDB_id)
    for chain in data['chain'].keys():
        zero_matrix = CreateZeroMatrix(data, chain)
        contact_matrix = CompleteWithContacts(data['chain'][chain]['aminoacids'], zero_matrix)
        vector = TransformIntoVector(contact_matrix)
        AppendToFASTAFile(PDB_id, chain, vector)
        
def ReadJSONFiles(filename):
    """reads JSON filenames from a file with the given filename
    filename - the name of a file which contains all JSON filenames"""
    path = os.path.join(os.getcwd(), filename)
    with open(path, 'r') as f:
        for line in f:
            AminoAcidsContacts(line.strip()) # pozbywa sie znakow nowej linii

if __name__ == '__main__':
    ReadJSONFiles('json_filenames.txt')