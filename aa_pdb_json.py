import requests
import json
import numpy as np
import os


def GetPDBFile(PDB_id):
    """downloads the PDB file of a structure with the given id as a text
    PDB_id - the id of a protein structure"""
    response = requests.get("https://files.rcsb.org/view/{}.pdb".format(PDB_id))
    if not response.ok:
        raise Exception("Protein ID {} is incorrect (required 4 characters) or this protein doesn't exit".format(PDB_id))
    PDB_file = response.text
    return PDB_file


def GetChains(PDB_file):
    """returns a list of chains present in the PDB structure
    PDB_file - the PDB file as a text (string)"""
    chains = []
    PDB_lines = PDB_file.split("\n")
    for line in PDB_lines:
        if not line.startswith('COMPND'):
            pass
        else:
            if line.startswith('COMPND   3 CHAIN:'):
                chains = line.split()
                chains = chains[3:] # od indeksu 3 zaczynają się litery łańcuchów
                for i, chain in enumerate(chains):
                    chains[i] = chain[0] # usuwa przecinki i sredniki
            if line.startswith('COMPND   4'): # niektore struktury maja tak wiele lancuchow ze sa w 2 linijkach np. 1QOH
                if 'FRAGMENT' not in line:
                    additional_chains = line.split()
                    additional_chains = additional_chains[2:]
                    for i, chain in enumerate(additional_chains):
                        additional_chains[i] = chain[0]
                    chains.extend(additional_chains)
                break # potem juz na pewno nie bedzie lancuchow
    return chains
                
def GetResiduesNum(PDB_file, chain):
    """returns number of all residues present in the given chain of a PDB structure
    PDB_file - the PDB file as a text (string)
    chain - the specific chain of a PDB structure"""
    PDB_lines = PDB_file.split("\n")
    for line in PDB_lines:
        if not line.startswith('DBREF'):
            pass
        else:
            line_split = line.split()
            if line_split[2] == str(chain):
                residue_number = int(line_split[4])
                return residue_number

def FindAlphaCarbonATOMS(PDB_file, chains, init_residue = 0, last_residue = 0):
    """finds all alpha carbons in the section ATOM of the given PDB_file
    PDB_file - the PDB file as a text (string)
    chains - chains of a PDB structure
    init_residue - the initial residue in a fragment you want to research
    last_residue - the last residue in a fragment you want to research"""
    alpha_carbons = {c: {} for c in chains} # jako punkt centralny aminokwasu przyjmuje wegiel alfa
    PDB_lines = PDB_file.split("\n")
    for line in PDB_lines:
        if not line.startswith('ATOM'): # pomijam wszystkie linie niezaczynajace sie od 'ATOM', bo wspolrzedne znajduje sie w sekcji 'MODEL'
            pass
        else:
            line_split = line.split()
            if init_residue == 0 and last_residue == 0: # cala struktura
                for c in chains: # analizujemy dana linijke dla kazdego lancucha
                    if line_split[4] == c and line_split[2] == "CA":
                        alpha_carbons[c][int(line_split[5])] = line_split
            else: # jeżeli chcemy zbadac fragment łańcucha
                for c in chains:
                    if int(line_split[5]) >= init_residue and int(line_split[5]) <= last_residue:
                        if line_split[4] == c and line_split[2] == "CA":
                            alpha_carbons[c][int(line_split[5])] = line_split
    return alpha_carbons


def EuclideanDistance(atom1, atom2):
    """calculates the euclidean distance of two atoms
    atom1 - list of coordinates of atom 1
    atom2 - list of coordinates of atom 2"""
    atom1_x, atom1_y, atom1_z = float(atom1[0]), float(atom1[1]), float(atom1[2]) # koordynaty sa w angstremach
    atom2_x, atom2_y, atom2_z = float(atom2[0]), float(atom2[1]), float(atom2[2])
    return np.sqrt((atom1_x - atom2_x)**2 + (atom1_y - atom2_y)**2 + (atom1_z - atom2_z)**2)


def FindNeighbours(carbon, alpha_carbons, dist, whole_structure = 1):
    """find neighbours for the given carbon among other alpha carbons at a given distance
    carbon - atom which you want to find neighbours for
    alpha_carbons - list of alpha carbons
    dist - maximal distance between a neighbour and the carbon
    whole_structure - a flag if you research a whole (1) or a part (0) of a structure"""
    neighbours = {}
    if whole_structure == 1:
        for i in range(int(carbon[5])+1, max(alpha_carbons.keys())+1): # mialo byc bez powtorzen par np. 1 i 10 to to samo co 10 i 1, wiec juz przy 10 nie sprawdza
            try: 
                eucl_dist = EuclideanDistance(carbon[6:9], alpha_carbons[i][6:9])
                if eucl_dist <= dist:
                    neighbours[i] = alpha_carbons[i][3]
                else:
                    pass
            except KeyError:
                pass
    else:
        for i in range(1, max(alpha_carbons.keys())+1):
            try: # ten blok zostal dodany, gdyz w sekcji ATOM nie bylo ciaglosci pomiedzy atomami (np. brakowalo atomu 288)
                if i == int(carbon[5]): # atom od samego siebie jest rowny 0, odrzucamy je
                    continue
                eucl_dist = EuclideanDistance(carbon[6:9], alpha_carbons[i][6:9])
                if eucl_dist <= dist:
                    neighbours[i] = alpha_carbons[i][3]
                else:
                    pass
            except KeyError:
                pass
    return neighbours


def RemoveRepetitions(aa_neighbours, chain, init_residue, last_residue):
    """removes repeated pairs of neighbours from list of aminoacids' neighbours
    aa_neighbours - list of aminoacids' neighbours which you want to clear from repetitions
    init_residue - the initial residue in a fragment you want to research
    last_residue - the last residue in a fragment you want to research
    """
    for i in range(init_residue+1, last_residue+1): 
        for j in range(init_residue, i): # tylko te z zakresu pomiedzy init a last beda sie powtarzaly ze soba, np. 14 i 15 oraz 15 i 14
            if j in aa_neighbours['chain'][chain]['aminoacids'][str(i)]['neighbours']:
                aa_neighbours['chain'][chain]['aminoacids'][str(i)]['neighbours'].pop(j)
    return aa_neighbours

def SaveAsJSON(PDB_id, aa_neighbours):
    """saves the result as a JSON file
    PDB_id - the id of a PDB structure
    aa_neighbours - list of aminoacids' neighbours (without repetitions)"""
    with open('{}_neighbours.json'.format(PDB_id), 'w') as f:
        json.dump(aa_neighbours, f)
        
def AminoAcidsNeighbours(PDB_id, dist, chain = "all", init_residue = 0, last_residue = 0):
    """the main program which contains all functions above & searches aminoacids' neighbours
    in the given PDB structure
    PDB_id - the id of a PDB structure
    dist - maximal distance between neighbours and alpha carbons of aminocids
    chain - the chain of a PDB structure (define if you want specific one)
    init_residue - the initial residue in a fragment you want to research (if value is 0, then analyses all)
    last_residue - the last residue in a fragment you want to research (if value is 0, then analyses all)"""
    PDB_file = GetPDBFile(PDB_id)
    aa_neighbours = {'PDB_id': PDB_id, 'chain': {}}
    # tutaj bylo aminoacids jeszcze, ale teraz bedziemy wyszukiwac po lancuchu w pliku JSON
    chains = GetChains(PDB_file) if chain == "all" else chain.upper()
    # jak all to zeby pobralo te lancuchy, a jak nie to tylko ten, ktory mamy badac (mozna teraz tez podac chain jako ciag znakow np. BC (lancuchy B i C) i tez bedzie dzialac)
    all_alpha_carbons = FindAlphaCarbonATOMS(PDB_file, chains)
    # szuka wszystkich atomow wegli alfa dla podanych lancuchow
    alpha_carbons = FindAlphaCarbonATOMS(PDB_file, chains, init_residue, last_residue) if init_residue != 0 and last_residue != 0 else all_alpha_carbons
    # tutaj wywoluje funkcje tylko gdy chce badac fragment struktury, a jak wszystkie atomy wegla alfa to all_alpha_carbons (to co juz zostalo wywolane)
    for c in chains: # iteracja po kazdym lancuchu
        res_num = GetResiduesNum(PDB_file, c)
        aa_neighbours['chain'][c] = {'res_num': res_num, 'aminoacids': {}}
        residues = range(1, max(all_alpha_carbons[c].keys())+1) if init_residue == 0 and last_residue == 0 else range(init_residue, last_residue+1)
        # to dla uproszczenia zeby nie robic dwa razy petli w kodzie za pomoca if i else tylko od razu poleci (tworze iterator przed petla)
        for i in residues:
            try: # nieciaglosc struktur wymusza blok try except
                aa_neighbours['chain'][c]['aminoacids'][str(i)] = {'name': alpha_carbons[c][i][3]}
                whole_structure = 1 if init_residue == 0 and last_residue == 0 else 0 # potrzebne do funkcji FindNeighbours, jak 1 to cala struktura, a jak 0 to nie
                aa_neighbours['chain'][c]['aminoacids'][str(i)]['neighbours'] = FindNeighbours(alpha_carbons[c][i], all_alpha_carbons[c], dist, whole_structure)
                # tutaj FindNeighbours bada oddzialywania tylko wewnatrz lancucha c
                if init_residue != 0 and last_residue != 0:
                    RemoveRepetitions(aa_neighbours, c, init_residue, last_residue)
            except KeyError:
                pass
        aa_neighbours['chain'][c]['aminoacids'] = sorted(aa_neighbours['chain'][c]['aminoacids'].items(), key = lambda item: int(item[0])) # sortowanie dla kazdego lancucha
        if int(aa_neighbours['chain'][c]['aminoacids'][-1][0]) > res_num:
            # na wypadek gdyby jakies aminokwasy wykraczaly poza liczbe reszt,
            # np. aminokwasy sa indeksowane 271, 272, ... 275 miedzy aminokwasami nr 27 a nr 28
            aa_neighbours['chain'][c]['res_num'] = int(aa_neighbours['chain'][c]['aminoacids'][-1][0])
    SaveAsJSON(PDB_id, aa_neighbours)
    
def ReadPDBEntries(filename):
    """reads PDB entries from a file with the given filename
    filename - the name of a file which contains all PDB entries"""
    path = os.path.join(os.getcwd(), filename)
    with open(path, 'r') as f:
        for line in f:
            print(line)
            line = line.strip() # w starszych wersjach s
            PDB_entries = line.split()
            if len(PDB_entries) == 2:
                AminoAcidsNeighbours(PDB_entries[0], int(PDB_entries[1]))
            elif len(PDB_entries) == 3:
                AminoAcidsNeighbours(PDB_entries[0], int(PDB_entries[1]), PDB_entries[2])
            elif len(PDB_entries) == 5:
                AminoAcidsNeighbours(PDB_entries[0], int(PDB_entries[1]), PDB_entries[2], int(PDB_entries[3]), int(PDB_entries[4]))
       

if __name__ == '__main__':
    ReadPDBEntries("pdb_entries.txt")